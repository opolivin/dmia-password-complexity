# dmia-password-complexity

This is a project to predict password complexity for a given password.

Here is the link to the [kaggle competition](https://www.kaggle.com/t/4b03db9d284240efb8c337d11c91dd30).

Link to the original repository of [Data Mininig in Action: Production ML](https://github.com/data-mining-in-action/DMIA_ProductionML_2021_Spring).

You can find the running application on www.olegpolivin.com.

And here is the link to the [running application deployed on Heroku](https://dmia-password-complexity-app.herokuapp.com).
See the `heroku_deploy` branch for the `.gitlab-ci.yml` settings.

For application deployment using Docker I did the following:

1. Manually created an app in Heroku interface
2. Manually `heroku stack:set container -a dmia-pass-complexity-docker`
3. Deployment is done by pushing to the `heroku_deploy_docker` branch.

## Introduction

In this repository I create a simple Flask application that predicts a password complexity given a password. This `README` consists of two parts. In the first part, I explain how to set up the virtual environment from scratch using `pyenv` and `pipenv`. It turned out quite tricky: `pipenv` wasn't working, it asked for a preinstalled version of `python`, and it turned out to be much more complicated to set up than `Anaconda`. However, I still liked a lot that all necessary packages are collected in a `Pipfile`, and you don't have a long (a very long) list of dependencies, like when you do `pip freeze`. That's nice, and that's why I plan to continue with `pipenv`.

The second part is about providing some commands to run the model in this repository.

### Setting up the virtual environment using `pipenv`.

Surely, there are many ways to do it, but below are the steps that worked and didn't work for me. First, it seemed that having `Anaconda` as my default package manager was not compatible with `pipenv`, and so

0. I commented all lines related to `Anaconda` path in `~/.bashrc` file. 

1. This didn't work for me: `sudo apt install pipenv`.

It installed the `pipenv`, but later at the step when I tried to do `pipenv install pandas` I got an error, and could not move further. It turned out that `pipenv` needs an installed version of `python` on your computer. Before I used `Anaconda` to manage `python` versions, but `pipenv` does not have this. Therefore, I decided to install a different `python` version manager: `pyenv`. 

2. [Instructions for installing](https://github.com/pyenv/pyenv) `pyenv` on `Ubuntu` worked perfectly well for me.

For our project we need to use `python 3.8.3`:
```
pyenv install 3.8.3 
```

3. So, in the end I installed `pipenv` using command.
```
pip install --user pipenv
```

And at this point I discovered that `pipenv` does not work for me. The reason was that I needed to add the `user base`'s binary directory to the `PATH`. Interestingly, a [good instruction](https://manpages.ubuntu.com/manpages/eoan/man1/pipenv.1.html) on how to do it specifically for `pipenv` is found on the Ubuntu manuals page.

4. Got to a local folder on the computer, and cloned (this) empty repository:
- `git clone https://gitlab.com/opolivin/dmia-password-complexity.git`
- `pipenv install --python 3.8.3`
- install all other necessary packages

### Working with the repository

Imagine you want to run the model on your own server. Here are some commands that will get you running.

Clone the repository:
```
git clone https://gitlab.com/opolivin/dmia-password-complexity.git
```

You will also need to download the list of [1_000_000 most common passwords](https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-1000000.txt), and put it into `dmia-password-complexity/data` folder.

If you want to train the model, then you will need to download some data, and my final repository structure is as follows.


```
dmia-password-complexity/
├── package
│   ├── app.py
│   ├── catboost_model
│   │   ├── preprocess.py
│   │   └── TheMightyModel.py
│   ├── config.yaml
│   ├── data
│   │   ├── 10-million-password-list-top-1000000.txt
│   │   ├── sample_submission.csv
│   │   ├── train.csv
│   │   └── Xtest.csv
│   ├── data_preprocessed
│   │   ├── scaler_y.pickle
│   │   ├── test_features.csv
│   │   └── train_features.csv
│   ├── outputs
│   ├── templates
│   │   └── index.html
│   └── trained_model
│       ├── catboost_password
│       └── submission.csv
├── Pipfile
├── Pipfile.lock
└── README.md



```
Install `pipenv` and 
```
cd dmia-password-complexity
```

`config.yaml` contains all the configuration that's necessary to run the code. However, you might need to change the `base_dir` paramters in the file. The `base_dir` path should contain the full path to the `dmia-password-complexity/package` directory where you cloned the repo.

For the application to run, you will need to have the packages mentionned in the `Pipfile` to be installed in the virtual environment.
So,
```
pipenv install
```

Make sure the following commands are run in the virual environment (`pipenv shell`).

#### Running the Flask application

The following command will run the Flask application on a `0.0.0.0`, `port=5000` host.

```
cd package
python app.py
```

If you want to train the model, you will need to download the data from the [kaggle competition](https://www.kaggle.com/c/dmia-production-ml-2021-1-passwords/), and put it into the `data` folder.

<h4> Preprocess the data and train the model <h4/>

```
cd package
python catboost_model/preprocess.py
python catboost_model/TheMightyModel.py command=fit
```


#### Predict a password frequency for a given password

```
cd package
python catboost_model/TheMightyModel.py command=predict password='123456'
```

#### Predict a submit for this kaggle competition

```
cd package
python catboost_model/TheMightyModel.py command=submit
```


