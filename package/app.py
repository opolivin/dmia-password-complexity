"""Flask application to predict password frequency"""

import pandas as pd

from flask import Flask, abort, jsonify, request, render_template
from catboost_model.TheMightyModel import TheMightyModel
from hydra.experimental import compose, initialize
from omegaconf import DictConfig




def load_model_and_data(cfg: DictConfig):

    base_dir = cfg.base.base_dir
    data_path = base_dir + cfg.preprocessing.raw_data_path
    CatboostRegressor = TheMightyModel(cfg)
    common_pass = pd.read_csv(
        data_path + '10-million-password-list-top-1000000.txt',
        header=None)
    common_pass = common_pass.reset_index()
    common_pass.columns = ['rank', 'Password']
    common_pass_dct = pd.Series(
        common_pass['rank'].values,
        index=common_pass['Password']).to_dict()
    return CatboostRegressor, common_pass_dct


def create_app():
    """Script to create the application.

    Returns:
        None.
    """


    app = Flask(__name__)

    @app.route("/", methods=["GET", "POST"])
    def index():
        """Main form rendering"""
        if request.method == 'POST':
            pw = request.form["password"]
            if pw == '':
                return render_template(
                    "index.html",
                    password='Now it is an empty string. Try to be creative!',
                    prediction='Empty strings are not used that often')
            pass_freq = CatboostRegressor.predict(pw, common_pass_dct)
            pass_freq = round(pass_freq, 2)
            return render_template("index.html", password=pw, prediction=pass_freq)
        else:
            return render_template("index.html")


    @app.errorhandler(500)
    def internal_server_error(error):
        return jsonify({
            "success": False,
            "error": 500,
            "message": "It's not you, it's us"
        }), 500

    return app


initialize(config_path='.')
cfg = compose(
    config_name="config")
CatboostRegressor, common_pass_dct = load_model_and_data(cfg)

APP = create_app()


if __name__ == '__main__':
    APP.run(host='0.0.0.0', threaded=False, port=5000, debug=False)
