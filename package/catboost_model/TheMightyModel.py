"""Catboost fit/predict class.
"""

from typing import Dict, List, Tuple
import pickle
import re

import numpy as np
import pandas as pd
import hydra

from catboost import CatBoostRegressor, Pool
from omegaconf import DictConfig


def rmsle(y_pred: List, y_true: List) -> float:
    """RMSLE metrics implementation.

    Arguments:
        y_pred {List} -- Predicted number.
        y_true {List} -- True numbr of password frequency.

    Returns:
        float -- RMSLE
    """
    return np.sqrt(np.square(np.log(y_pred+1) - np.log(y_true+1)).mean())


def read_data(cfg: DictConfig) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Read preprocessed data.

    Arguments:
        cfg {DictConfig} -- Hydra config.

    Returns:
        Tuple[pd.DataFrame, pd.DataFrame] -- Train/Test after preprocessing.
    """
    base_dir = cfg.base.base_dir
    data_preprocessed_path = base_dir + cfg.training.preprocessed_data_path
    train_filename = cfg.training.train_preprocessed
    test_filename = cfg.training.test_preprocessed
    scaler_name = cfg.preprocessing.scaler_label_pickle_name

    train_path = data_preprocessed_path + train_filename
    test_path = data_preprocessed_path + test_filename
    scaler_path = data_preprocessed_path + scaler_name

    train = pd.read_csv(train_path)
    test = pd.read_csv(test_path)

    with open(scaler_path, 'rb') as fp:
        scaler_y = pickle.load(fp)

    return train, test, scaler_y

# Taken from stackoverflow
# https://stackoverflow.com/questions/16709638/checking-the-strength-of-a-password-how-to-check-conditions
def password_check(password):
    """
    Verify the strength of 'password'
    Returns a dict indicating the wrong criteria
    A password is considered strong if:
        8 characters length or more
        1 digit or more
        1 symbol or more
        1 uppercase letter or more
        1 lowercase letter or more
    """

    # calculating the length
    length_error = len(password) < 8

    # searching for digits
    digit_error = re.search(r"\d", password) is None

    # searching for uppercase
    uppercase_error = re.search(r"[A-Z]", password) is None

    # searching for lowercase
    lowercase_error = re.search(r"[a-z]", password) is None

    # searching for symbols
    symbol_error = re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password) is None

    # overall result
    password_ok = not ( length_error or digit_error or\
        uppercase_error or lowercase_error or symbol_error)

    return (
        length_error,
        digit_error,
        uppercase_error,
        lowercase_error,
        symbol_error,
        password_ok)


def get_features_for_a_string(
    password: str,
    common_pass_dct: Dict) -> np.ndarray:
    """Function to get features for a string password.
    These features are later used in Catboost model.

    Arguments:
        password {str} -- Password string.
        common_pass_dct {Dict} -- Dictionary of common passwords.

    Returns:
        np.ndarray -- Features in a numpy array.
    """
    length = len(password)

    (
        length_error,
        digit_error,
        uppercase_error,
        lowercase_error,
        symbol_error,
        password_ok
        ) = password_check(password)

    error_count = length_error + digit_error +\
        uppercase_error + lowercase_error + symbol_error

    try:
        rank = common_pass_dct[password]
        is_among_common = 1
    except KeyError:
        rank = 903754
        is_among_common = 0

    array = np.array((
        length,
        length_error,
        digit_error,
        uppercase_error,
        lowercase_error,
        symbol_error,
        password_ok,
        error_count,
        is_among_common,
        rank))
    return array


class TheMightyModel:
    """Class for the Catboost model.
    It implements 'fit' and 'predict' methods.
    """

    def __init__(self, cfg: DictConfig):

        self.cfg = cfg

    def get_features_labels(self, cfg: DictConfig)\
        -> Tuple[pd.DataFrame, np.ndarray]:
        """Function to get features from train data.

        Arguments:
            cfg {DictConfig} -- Hydra config.

        Returns:
            Tuple[pd.DataFrame, np.ndarray] -- Features and labels.
        """

        xcols = cfg.training.xcols
        train, _, scaler_y = read_data(cfg)
        features = train[xcols]
        labels = scaler_y.transform(train['Times'].values.reshape(-1,1))

        return features, labels


    def fit(self):
        """Fit the catboost model.

        Returns:
            [Catboost model] -- Fitted catboost model.
        """

        cfg = self.cfg
        features, labels = self.get_features_labels(cfg)

        catboost_params = cfg.training.catboost_params
        cat_features = cfg.training.catboost_params.cat_features

        cbr = CatBoostRegressor(**catboost_params)
        train_df = Pool(
            data=features,
            label=labels,
            cat_features=cat_features
        )
        cbr.fit(train_df)
        return cbr


    def predict(
        self,
        password: str,
        common_passwords_dct: Dict) -> float:
        """Makes prediction given a password.

        Arguments:
            password {str} -- Password to check.
            common_passwords_dct {Dict} -- Dict with common passwords.

        Returns:
            [float] -- Predicted number of times
            one could encounter that password.
        """
        cfg = self.cfg
        base_dir = cfg.base.base_dir
        output_dir = base_dir + cfg.training.output_dir
        model_name = cfg.training.model_name
        model_path = output_dir + model_name

        data_preprocessed_path = base_dir + cfg.training.preprocessed_data_path
        scaler_name = cfg.preprocessing.scaler_label_pickle_name
        scaler_path = data_preprocessed_path + scaler_name

        catboostmodel = CatBoostRegressor()
        catboostmodel.load_model(model_path)

        with open(scaler_path, 'rb') as fp:
            scaler_y = pickle.load(fp)

        array = get_features_for_a_string(password, common_passwords_dct)
        pass_freq_scaled = catboostmodel.predict(array)
        pass_freq = scaler_y.inverse_transform(pass_freq_scaled.reshape(-1))[0]
        return pass_freq

    def submit(self, submission_name):
        cfg = self.cfg
        base_dir = cfg.base.base_dir
        data_path = base_dir + cfg.preprocessing.raw_data_path

        xcols = cfg.training.xcols
        cat_features = cfg.training.catboost_params.cat_features

        output_dir = base_dir + cfg.training.output_dir
        model_name = cfg.training.model_name
        model_path = output_dir + model_name

        _, test, scaler_y = read_data(cfg)
        features_test = test[xcols]

        test_df = Pool(
            data=features_test,
            cat_features=cat_features
            )

        catboostmodel = CatBoostRegressor()
        catboostmodel.load_model(model_path)

        pred = catboostmodel.predict(test_df).reshape(-1,1)
        y_pred = scaler_y.inverse_transform(pred)

        preds = list(y_pred.flatten())
        preds.append(3)


        subm = pd.read_csv(data_path + 'sample_submission.csv')
        subm['Times'] = preds
        subm.to_csv(output_dir + submission_name, index=False)


@hydra.main(config_path='..', config_name='config')
def run_model(cfg: DictConfig):
    """Function to run the model: fit or predict.

    Arguments:
        cfg {DictConfig} -- Hydra config.
    """
    command = cfg.command
    save_model = cfg.training.save_model
    base_dir = cfg.base.base_dir
    data_path = base_dir + cfg.preprocessing.raw_data_path

    common_pass = pd.read_csv(
        data_path + '10-million-password-list-top-1000000.txt',
        header=None)
    common_pass = common_pass.reset_index()
    common_pass.columns = ['rank', 'Password']
    common_pass_dct = pd.Series(
        common_pass['rank'].values,
        index=common_pass['Password']).to_dict()

    if command == 'fit':
        CatBoostModel = TheMightyModel(cfg)
        catboostmodel = CatBoostModel.fit()

        if save_model:
            output_dir = cfg.base.base_dir + cfg.training.output_dir
            model_name = cfg.training.model_name
            catboostmodel.save_model(output_dir + model_name)

    if command == 'predict':
        CatBoostModel = TheMightyModel(cfg)
        password_str = str(cfg.password)
        times_pred = CatBoostModel.predict(password_str, common_pass_dct)
        print(times_pred)

    if command == 'submit':
        CatBoostModel = TheMightyModel(cfg)
        CatBoostModel.submit(submission_name='submission.csv')


if __name__ == '__main__':
    run_model()
