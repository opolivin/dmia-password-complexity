"""Script to preprocess the raw data and save it to file."""

import pickle
import re

from typing import Tuple

import hydra
import pandas as pd

from omegaconf import DictConfig
from sklearn.preprocessing import StandardScaler



def read_data(cfg: DictConfig)\
    -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """Function to read the train/test data.
    It also reads the 10 million passwords file.

    Arguments:
        cfg {DictConfig} -- Hydra config.

    Returns:
        Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame] --
        Train/Test/MostCommon dataframe
    """
    base_dir = cfg.base.base_dir
    data_path = base_dir + cfg.preprocessing.raw_data_path

    train = pd.read_csv(data_path + 'train.csv')
    train = train[train['Password'].notnull()].reset_index(drop=True)

    test = pd.read_csv(data_path + 'Xtest.csv')
    test = test[test['Password'].notnull()].reset_index(drop=True)

    common_pass = pd.read_csv(
        data_path + '10-million-password-list-top-1000000.txt',
        header=None)
    common_pass = common_pass.reset_index()
    common_pass.columns = ['rank', 'Password']

    return train, test, common_pass


def symb_error(password: str) -> bool:
    """Function to check whether there is a special
    symbol in the password or not.

    Arguments:
        password {str} -- String password.

    Returns:
        bool -- [description]
    """
    error = re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password) is None
    return error


def featurise(
    cfg: DictConfig,
    df: pd.DataFrame,
    filename: str,
    common_pass_df: pd.DataFrame) -> None:
    """Function to prepare features for a given dataframe.
    It could be train or test.

    Arguments:
        cfg {DictConfig} -- Hydra config.
        df {pd.DataFrame} -- Train/test dataframe.
        filename {str} -- Filename to save data.
        common_pass_df {pd.DataFrame} -- Dataframe with common passwords.

    Return:
        None. Featurized datasets are saved to disk.
    """
    base_dir = cfg.base.base_dir
    data_preprocessed_path = base_dir + cfg.preprocessing.preprocessed_data_path
    cols = cfg.preprocessing.cols

    # Features related to the strength of the password.
    df['length'] = df['Password'].str.len()
    df['length_error'] = (df['Password'].str.len() < 8).astype(int)
    df['digit_error'] = 1 - df['Password'].str.contains("\d")
    df['uppercase_error'] = 1 - df['Password'].str.contains("[A-Z]")
    df['lowercase_error'] = 1 - df['Password'].str.contains("[a-z]")
    df['symbol_error'] = df['Password'].apply(symb_error).astype(int)
    df['error_count'] = df[cols].sum(axis=1)
    df['password_ok'] = (df['error_count'] == 0).astype(int)

    # Features related to the most common passwords database.
    df = pd.merge(df, common_pass_df, how='left')
    df['is_among_common'] = df['rank'].notnull().astype(int)
    df['rank'] = df['rank'].fillna(len(common_pass_df))

    df.to_csv(data_preprocessed_path + filename, index=False)


def scale_label(
    cfg: DictConfig,
    label: pd.Series) -> None:
    """Function to scale the label using StandardScaler.
    Saves results into pickle file.

    Arguments:
        cfg {DictConfig} -- Hydra config.
        label {pd.Series} -- Label values.
    """
    base_dir = cfg.base.base_dir
    data_preprocessed_path = base_dir + cfg.preprocessing.preprocessed_data_path
    scaler_name = cfg.preprocessing.scaler_label_pickle_name
    path_to_save = data_preprocessed_path + scaler_name

    scaler_y = StandardScaler()
    scaler_y.fit(label.values.reshape(-1,1))
    with open(path_to_save, 'wb') as fp:
        pickle.dump(scaler_y, fp)

@hydra.main(config_path='..', config_name='config')
def main(cfg: DictConfig):
    """Function to get train/test data, featurize, scale labels
    and save to disc.

    Arguments:
        cfg {DictConfig} -- Hydra config.
        label {pd.Series} -- Label values.
    """

    train_df, test_df, common_pass_df = read_data(cfg)
    featurise(cfg, train_df, 'train_features.csv', common_pass_df)
    featurise(cfg, test_df, 'test_features.csv', common_pass_df)
    scale_label(cfg, label = train_df['Times'])


if __name__ == '__main__':
    main()
